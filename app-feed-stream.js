AppStream = new Meteor.Stream('app-stream');

if (Meteor.isClient) {
	AppStream.truck = function (domain, name, message, detailsObject) {
		var doc = {
			domain: domain,
			name: name,
			user: Tafoukt.me(),
			message: message,
			details: detailsObject
		}
		console.log('truck', doc);
		return AppStream.emit('event', doc);
	};

	Feeds = new Meteor.Collection(null);

	function handle (data) {
		console.info('new event', data.domain, data.name, data.user, data.message, data.details);
		Feeds.insert(data)
	};
	AppStream.on('event', function (data) {
		handle(data);
		// console.info('new event', data.domain, data.name, data.user, data.message, data.details);
	});

	Template.feed.feeds = function () {
		return Feeds.find({}, {sort: {createdAt: -1}})
	}

} else {
	
	AppStream.permissions.write(function(eventName) {
	  // var userId = this.userId;
	  // var subscriptionId = this.subscriptionId;
	  // return true to accept and false to deny
	  return true;
	});

	AppStream.permissions.read(function(eventName) {
	  if (this.userId && Tafoukt.is(this.userId, "admin")) 
	  	return true;
	  else
	  	return false;
	});

	AppStream.addFilter(function (event, data) {
		data.createdAt = new Date;
		return data;
	})
};