ChatStream = new Meteor.Stream('chat-stream');

if (Meteor.isClient) {
	ChatStream.on('message', function (message) {
		console.log('new message', _.result(message, "message"), message)
	});

	ChatStream.send = function (message, to) {
		return ChatStream.emit('message', {
			from: Meteor.userId(),
			to: to,
			message: message
		});
	}

} else {
	ChatStream.permissions.write(function(eventName, args) {
		return this.userId ? true : false;
	});

	ChatStream.permissions.read(function(eventName, args) {
		console.log('read')
		if (! this.userId) 
	  	return false;
	  
	  if (Tafoukt.is(this.userId, "admin")) 
	  	return true;
	  console.log('to', args['to'], this.userId)
	  if (args['to'] === this.userId) 
	  	return true;
	  
	  return false;
	});

	// ChatStream.addFilter(function(eventName, args) {
	//   var to = _.result(args, "to")
	//   if (! to) {
	//   	args.to = _.result(Meteor.users.findOne({admin: true}), "_id")
	//   }
	//   return args
	// });
}